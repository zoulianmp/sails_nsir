# **Safety and Incident Learning System (NSIR-RT)** #

This is the repository for the NSIR-RT compatible version of the Safety and Incident Learning System (SaILS). This version, developed at the McGill University Health Centre, incorporates the taxonomy of the Canadian National System for Incident Reporting - Radiation Treatment (NSIR-RT). For the original version of SaILS, developed at the Ottawa Hospital Cancer Centre, please visit: [https://bitbucket.org/tohccmedphys/sails/](https://bitbucket.org/tohccmedphys/sails/).

### The core features offered by SaILS are: ###

1. An incident reporting interface
2. An incident investigation interface
3. Incident tracking functionality
4. A data visualization interface

### Additional features within SaILS include: ###

* An administrator interface
* An email framework (with support for automatic email reminders)
* Framework for connecting SaILS with your electronic medical record (for auto-field population & document retrieval)
* Taskable actions
* Incident templates
* User dashboards
* A robust incident search feature
* Filtered incident lists

### Installation Instructions ###

* Clone this repository into your server (this will make a local copy of the repository)
* Follow the instructions provided in the *SaILS_Installation_Instructions.pdf* file included in the source code (in the root directory of the project)
* Please contact us if you have trouble getting the software installed
* Note: The instructions are straightforward, but basic familiarity with Bash and Python will be required to install and configure the software to your needs!